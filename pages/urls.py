from django.urls import path
from . import views
from .views import CreateImageView, CreateImageView2

urlpatterns = [
    path('',views.index, name="index"),
    path('aboutus/',views.aboutus, name="aboutus"),
    path('uploadenc/',CreateImageView.as_view(), name="uploadenc"),
    path('uploaddec/',CreateImageView2.as_view(), name="uploaddec"),
    path('encrypt/',views.encImage, name="encrypt"),
    path('decrypt/',views.decImage, name="decrypt"),
]
